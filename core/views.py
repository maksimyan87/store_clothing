from django.shortcuts import render

from core.models import ProductCategory, Product


def index(request):
    context = {
        'title': 'Store',
        'is_promotion': True,
    }
    return render(request, 'core/index.html', context)


def products(requests):
    context = {
        'title': 'Store - Каталог',
        'products': Product.objects.all(),
        'categories': ProductCategory.objects.all()
    }
    return render(requests, 'core/products.html', context)
