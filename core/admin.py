from django.contrib import admin

from core.models import ProductCategory, Product

admin.site.register(Product)
admin.site.register(ProductCategory)